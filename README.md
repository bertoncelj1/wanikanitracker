### Info
Downloades data from Wanikani API and saves it.
If the data has not changed than it will not save.

### Requirements
Program requires 'jq' to run. You can download it with 'apt install jq'.
It is used to parse downloaded JSON files. And extract user's level from it.

### Before your start
Before you start the program make sure that all of the settings in **settings.sh** file are set. Like:
 * userApi and
 * baseDir.

### Periodically download
Is best to set crontab to run this program.
type crontab -e into your terminal and than add the following lines:
```sh
# m      h    dom    mon    dow   command
  */20   *    *      *      *     PATH/TO/PROGRAM/wanikaniTracker.sh
  12     */2  *      *      *     PATH/TO/PROGRAM/wanikaniTracker.sh all
```

This will make sure that everything is going to be downloaded every twenty minutes. Kanji, vocubalary and radicals every 2 hours, since they take more space.
