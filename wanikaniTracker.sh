#!/bin/bash

# find where script is beeing run from
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/settings.sh


version="v1.4"
baseUrl="https://www.wanikani.com/api/"$version"/user/"$userApi
DEBUG=""


# check if user entered his API key
[[ -z "$userApi" ]] && { echo "Please set your API key in settings.sh file."; exit 1; }


LOGERR=true
errLogFile="$baseDir"/"error.log"

main() {
    
    # set debug mode if one of the first parameters is debug
    if [[ "$1" == "debug" ]] || [[ "$2" == "debug" ]]; then 
        DEBUG="true"
    fi
        
    # if argument "all" is set than all recicals, kanji and vocabulary is downloaded
    if [[ "$1" == "all" ]]; then
	    getUserLevel; level=$?
        
        # check if we were able to get level
        [[ "$level" -eq 255 ]] && logError "Unable to get level. Level:'$level'" && return
        
        # create sequance of numbers from 1 to $level. like: 1,2,3,4,5,6,7,8 ...
        levels=$(seq -s, 1 1 $level)

        get "radicals" "radicals/$levels"
        get "kanji" "kanji/$levels"
        get "vocabulary" "vocabulary/$levels"
        
        return
    fi
    
    
    callType="srs-distribution"
    get $callType $callType
    
    callType="study-queue"
    get $callType $callType

    callType="critical-items"
    get $callType $callType/75
    
    callType="recent-unlocks"
    get $callType $callType/10
    
    callType="level-progression"
    get $callType $callType
}


debug() {
    if ! [[ $DEBUG ]]; then return; fi
    
    echo $@
}

logError() {
    # output debug message 
    debug $@

    if ! [[ $LOGERR ]]; then return; fi
    
    echo $(date -u) $@ >> $errLogFile
}

# checkConnection $url 
#
# check connection by using wget
chekcConnection() {
    url="$1"    
    
    wget -q --spider $url

    hasConnection=$?
    
    if [ $hasConnection -eq 0 ]; then
        debug "OK connection to website: " $url
    else
        logError "NO connection to website: " $url
    fi
    
    return $hasConnection
}

# checkConnectionPing $url
#
# ping given url
checkConnectionPing() {
    hostName="$1"

    ping -c 1 $hostName &> /dev/null;
    
    hasConnection=$?
    
    if [ $hasConnection -eq 0 ]; then
        debug "ping OK: $hostName"
    else
        debug "ping ERROR $hasConnection: $hostName"
    fi
    
    return $hasConnection
}

# checkConnectionInternet <no arguments>
#
# check connection to the internet by first pinging well known websites
# than pinging gateway
checkConnectionInternet() {
    # ping some big websites
    checkConnectionPing www.stackoverflow.com ||
    checkConnectionPing www.yahoo.com ||
    checkConnectionPing www.twitter.com ||
    checkConnectionPing www.google.com
    
    pingStatus=$?
    
    if [ "$pingStatus" -eq 0 ]; then
        debug "Internet connection OK"
    else
    
        # check connection to the gateway
        checkConnectionPing "192.168.1.1"
        
        pingStatusGateway=$?
        if [ "$pingStatusGateway" -eq 0 ]; then
            # TODO if that is the case Rpi should set out some kind of alarm to warn user to fix internet
            logError "Able to acces gateway. Something is wrong with ISP"
        else
            logError "Unable to access gateway. Something is wrong with cable or router!"
        fi
        
        logError "NO internet connection! ping status:$pingStatus"
    fi
    
    return $pingStatus
}


# getUserLevel <no arguments>
#
# get user level from user_information API call so that correct amount of radicals, kanji an vocubalary can be downloaded.
# the level is returned in return value.
# if any error occured 255 is returned insted
getUserLevel() {
    debug "Getting user level"

    command -v jq >/dev/null 2>&1 || { echo >&2 "I require jq but it's not installed.  Aborting."; exit 1; }

    # download user inforamtion from api
    download "$baseUrl/user-information"

    # if no content was downloaded return
    
    [[ -n "$downloadContent" ]] || return 255
    
    # extract user's level from downloaded JSON 
    level=$(jq '.user_information.level' <(echo $downloadContent)) 
    
    
    rValue=$?
    if [[ "$rValue" -ne 0 ]]; then
        logError "Unable to parse JSON. Return value: $rValue. Level '$level', DownloadedContent:'$downloadContent'"
        return 255
    fi
    if [[ -z "$level" ]] || [[ "null" == "$level" ]]; then 
        logError "Unable to extract user level: '$level', DownloadedContent:'$downloadContent'"
        return 255
    fi

    debug "Got user level: '$level'"
    return $level
}

# download $url
#
# downloads content from specific url (parameter $1). Ff content is empty than it tries again
# downloaded content is returned in varable $downloadContent
downloadContent=""
download() {
    url="$1"
    
    tries="5"
    while [[ $tries > 0 ]]; do
       tries=$((tries - 1))
       
       debug "Trying download for:$url try:$tries"
       # download
       downloadContent="$(wget $url -q -O -)"
        
       debug "got ${#downloadContent} characters"
       
       # if we got some content than finsih
       [[ -z "$downloadContent" ]] || return $(true)
       
       
       debug "downloaded content was empty waiting ...."
       logError "Can not download try:$tries url:$url got:'$downloadedContent'"
       
       # wait 10 seconds before trying to download again
       sleep 10
    done
    
    # check internet connection
    checkConnectionInternet

    if [ $? -eq 0 ]; then
    
        # if it has internet conncetion than check connection to the url
        chekcConnection $url
        
        if [ $? -eq 0 ]; then
            logError "Able to connect to $url"    
        else
            logError "Has internet connection but no connection to url $url" 
        fi
    fi
    
    # unable to download after x tries
    return $(false)
}

# diffVarAndFile $var $file
#
# compares passed variable and file by creating sha1 sum of them
# if there is any difference 1 is returend otherwise 0
# 
diffVarAndFile() {
    var="$1"
    file=$2

    # create sha sum for both file and variable
    varSha=$(sha1sum <<< "$var" | cut -f1 -d' ')
    fileSha=$(sha1sum $file | cut -f1 -d' ')
    
    debug "variable sha: $varSha"
    debug "file sha    : $fileSha"
    
    [[ $varSha == $fileSha ]]
    return $?
}

# get $url $saveDir
#
# downlads api request (parameter $2) and saves it to directory (parameter $1)
# if last downloaded file is the same as newly downloaded it does nothing
# returns 0(true) if new file was created or 1(false) if no file was created
get() {
    directory=$baseDir/$1
    request=$2
    
    debug ""
    debug "Getting request $request"
    
    # creates directory
    mkdir -p $directory
    
    # download from api
    download "$baseUrl/$request"
    
    # no content was downloaded return
    [[ -n "$downloadContent" ]] || return $(false)
    
    
    # find the most recent file in directory
    lastGet=$(ls -t $directory | head -1);
        
    # check if lastGet exist and if lastGet file differs from downloded data
    
    if [[ -z "$lastGet" ]] || ! diffVarAndFile "$downloadContent" "$directory/$lastGet"; then
        
        debug "Saving new downloaded content"
        # saves the downloadeed content 
        fileName=$directory/$(date +%s).json
        echo "$downloadContent" > "$fileName"
        
        
        writeStatus=$?
        if [ "$writeStatus" -ne 0 ]; then
            logError "Unable to write file: '$fileName', error code: $writeStatus" 
        else
            debug "Write into file OK"
        fi
        
        
        return $(true)
    else
        debug "Downladed conent is the same"
        return $(false)
    fi
}

main $*


